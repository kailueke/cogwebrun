# CogWebRun

Simple app to control a Cog web browser window.

Cog is a single window browser without tabs and UI elements based on WPE WebKit.
The `fdo` platform is used at the moment.

## Usage

Enter the domain or URL in the text field and hit Enter.
The app will start a Cog window if there is none and will put the entered website to your bookmarks.
You can remove unwanted bookmarks by clicking the delete button.

To migrate existing bookmarks, place the URLs (or domain names) as lines in the `~/.local/share/cogwebrun/bookmarks` textfile.

In Phosh you can add the _CogWebRunBack_ app to your favorites as a shortcut.

Cookies are stored in `~/.local/share/cog/cookies.text`.

## Binary packages for Mobian/Debian

You can download the packages [here](https://gitlab.gnome.org/kailueke/cogwebrun/-/tags/0.4).

On, e.g., the PinePhone download the deb and install it with:

```
sudo apt install $(ls ./cogwebrun_*_all.deb | sort | tail -n 1)
```

## Installation from source:

```
# on Mobian/Debian:
sudo apt install make checkinstall
sudo make install-deb

# or generic:
sudo make install
```

## Deinstallation:

```
# on Mobian/Debian:
sudo dpkg -r cogwebrun

# or generic:
sudo make uninstall
```


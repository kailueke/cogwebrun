#!/usr/bin/env python3

# GPL-2+ ("and any later version")
# Kai Lüke 2021

import gi # Debian package: python3-gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib # Debian package: gir1.2-glib-2.0, gir1.2-gtk-3.0
import os, sys
from pathlib import Path

from subprocess import check_output

os.chdir(os.path.dirname(sys.argv[0]))
user_folder = str(Path.home()) + "/.local/share/cogwebrun"

line_height = 50

class CogWebRun(Gtk.Application):

  def __init__(self, application_id):
    Gtk.Application.__init__(self, application_id=application_id)

  def do_startup(self):
    Gtk.Application.do_startup(self)

  def open(self, url):
    # can be a domain without an http:// prefix
    url = url.strip()
    if url == "":
      url = "about:blank"
    try:
      check_output(["cogctl", "open", url])
    except Exception as e:
      print(e)
      _, _, _, _ = GLib.spawn_async(["/usr/bin/env", "cog", "-P", "fdo", "--cookie-jar=text", url], standard_output=-1, standard_input=-1, standard_error=-1)

  def on_bookmark(self, widget):
    self.open(widget.get_label())

  def on_addr(self, widget):
    domain = widget.get_text()
    self.open(domain)
    if domain.strip() == "" or domain in self.get_bookmarks():
      return
    self.add_bookmark(domain)
    self.window.show_all()
    with open(user_folder + "/bookmarks", "a") as f:
      f.write(domain+"\n")

  def add_bookmark(self, domain):
    wrapper = Gtk.HBox(halign=Gtk.Align.START)
    remove_bookmark = Gtk.Button.new_from_icon_name(icon_name="edit-delete-symbolic", size=1)
    remove_bookmark.connect("clicked", self.on_remove_bookmark)
    wrapper.add(remove_bookmark)
    button = Gtk.Button(label=domain, height_request=line_height, width_request=300)
    button.connect("clicked", self.on_bookmark)
    wrapper.add(button)
    self.bookmarks.add(wrapper)

  def on_prev(self, widget):
    check_output(["cogctl", "previous"])

  def on_reload(self, widget):
    check_output(["cogctl", "reload"])

  def on_next(self, widget):
    check_output(["cogctl", "next"])

  def get_bookmarks(self):
    return [child.get_children()[1].get_label() for child in self.bookmarks.get_children()]

  def on_remove_bookmark(self, widget):
    line = widget.get_label()
    mybox = widget.get_parent()
    self.bookmarks.remove(mybox)
    with open(user_folder + "/bookmarks", "w") as f:
      all = "\n".join(self.get_bookmarks())
      if all != "":
        all += "\n"
      f.write(all)

  def do_activate(self):
    list = Gtk.Application.get_windows(self)
    if list:
      list[0].present()
      return
    self.window = Gtk.Window(title="CogWebRun")
    self.window.set_default_size(350, 400)
    self.window.set_application(self)
    self.vbox = Gtk.VBox(valign=Gtk.Align.START)
    self.window.add(self.vbox)
    self.hbox = Gtk.HBox(homogeneous=True)
    self.vbox.add(self.hbox)
    self.prev = Gtk.Button(label="←", height_request=line_height)
    self.prev.connect("clicked", self.on_prev)
    self.hbox.add(self.prev)
    self.reload = Gtk.Button(label="⟳", height_request=line_height)
    self.reload.connect("clicked", self.on_reload)
    self.hbox.add(self.reload)
    self.next = Gtk.Button(label="→", height_request=line_height)
    self.next.connect("clicked", self.on_next)
    self.hbox.add(self.next)
    self.addr = Gtk.Entry()
    self.addr.connect("activate", self.on_addr)
    self.vbox.add(self.addr)
    self.scrolled_window = Gtk.ScrolledWindow(propagate_natural_height=True, propagate_natural_width=True)
    self.vbox.add(self.scrolled_window)
    self.view = Gtk.Viewport()
    self.scrolled_window.add(self.view)
    self.bookmarks = Gtk.VBox(valign=Gtk.Align.START)
    self.view.add(self.bookmarks)
    try:
      with open(user_folder + "/bookmarks", "r") as f:
        content = f.read()
      for line in [x.strip() for x in content.split("\n") if x.strip() != ""]:
        self.add_bookmark(line)
    except Exception as e:
      print(e)
    self.window.show_all()

  def on_window_destroy(self, window):
    self.quit()

  def quit_cb(self, action, parameter):
    self.on_window_destroy(self.window)


if __name__ == "__main__":
  check_output(["mkdir", "-p", user_folder])
  app = CogWebRun("org.gnome.gitlab.kailueke.CogWebRun")
  exit_status = app.run(sys.argv)
  sys.exit(exit_status)

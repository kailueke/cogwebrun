install:
	ls -lah ${DESTDIR}
	install -D -o root -g root -m 755 cogwebrun ${DESTDIR}/usr/bin/cogwebrun
	install -D -o root -g root -m 644 org.gnome.gitlab.kailueke.CogWebRun.desktop ${DESTDIR}/usr/share/applications/org.gnome.gitlab.kailueke.CogWebRun.desktop
	install -D -o root -g root -m 644 org.gnome.gitlab.kailueke.CogWebRunBack.desktop ${DESTDIR}/usr/share/applications/org.gnome.gitlab.kailueke.CogWebRunBack.desktop

uninstall:
	rm /usr/bin/cogwebrun
	rm /usr/share/applications/org.gnome.gitlab.kailueke.CogWebRun.desktop
	rm /usr/share/applications/org.gnome.gitlab.kailueke.CogWebRunBack.desktop

install-deb:
	checkinstall "--requires=cog, libglib2.0-bin, python3-gi, gir1.2-glib-2.0, gir1.2-gtk-3.0" --pkgarch=all --pkgname=cogwebrun --pkglicense=GPL-2+ --nodoc --pkgversion=0.4 --pkgrelease=0 --include=listfile --deldesc=yes --backup=no -y
